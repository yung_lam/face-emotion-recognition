#include "testApp.h"

using namespace ofxCv;
using namespace cv;

void testApp::setup() {
	ofSetLogLevel(OF_LOG_FATAL_ERROR);
	ofSetVerticalSync(true);
	cam.setVerbose(true);
	camWidth=320;
	camHeight=240;
	cam.initGrabber(camWidth, camHeight);
	videoMirror = new unsigned char[camWidth*camHeight*3];
	mirrorTexture.allocate(camWidth, camHeight, GL_RGB);
	tracker.setup();
	tracker.setRescale(.5);
	
	finder.setup("haarcascade_frontalface_default.xml");
	//finder.findHaarObjects(img);
	mask.loadImage("mask.png");
    mask.resize(PCA_WIDTH, PCA_HEIGHT);
    mask_pixels = mask.getPixels();
	rec.learn();
    gray.allocate(PCA_WIDTH, PCA_HEIGHT);
    color.allocate(PCA_WIDTH, PCA_HEIGHT);
	// PRECALCULATE TRANSLUCENT FACE SPRITES
    calcFaceSprites();
	rec.cascade=finder.cascade;
    showEigens=false;
    showFaces=false;
    showExtracted=false;
    showTest = false;
    showLeastSq=false;
    showClock=false;
    bgSubtract=false;
	expressionType="neutral";
	music.setMultiPlay(false);
	music.setVolume(0.8f);
    ofEnableAlphaBlending();
	arduinoCmd = ""; currentPersonName = "";
	serial.setup(1, 9600);
	//serial.startContinuousRead(false);
	ofSetFrameRate(30);
	countAbnormalPerson=0;
	stablePersonName="";
	messageSent = false;
/* */
}

void testApp::update() {
	cam.update();
	if(cam.isFrameNew()) {
		if(tracker.update(toCv(cam))) {
			classifier.classify(tracker);
		}
		 unsigned char * pixels = cam.getPixels();
		 /*for (int i = 0; i < camHeight; i++) {
			for (int j = 0; j < camWidth*3; j+=3) {
				// pixel number
				int pix1 = (i*camWidth*3) + j;
				int pix2 = (i*camWidth*3) + (j+1);
				int pix3 = (i*camWidth*3) + (j+2);
				// mirror pixel number
				int mir1 = (i*camWidth*3)+1 * (camWidth*3 - j-3);
				int mir2 = (i*camWidth*3)+1 * (camWidth*3 - j-2);
				int mir3 = (i*camWidth*3)+1 * (camWidth*3 - j-1);
				// swap pixels
				videoMirror[pix1] = pixels[mir1];
				videoMirror[pix2] = pixels[mir2];
				videoMirror[pix3] = pixels[mir3];	
			}
		}*/
		//mirrorTexture.loadData(videoMirror, camWidth, camHeight, GL_RGB);	

		img.setFromPixels(pixels, camWidth, camHeight, OF_IMAGE_COLOR, true);
		//img.update();
		test_image.setFromPixels(pixels, camWidth, camHeight, OF_IMAGE_COLOR);
        //test_image.resize(camWidth/TEST_DIV, camHeight/TEST_DIV);
        test_image.update();
		
        finder.findHaarObjects(test_image);

	}
}

void testApp::draw() {
	ofSetColor(255);
	cam.draw(0, 0);
	
	tracker.draw();
	/*    // display other items
    if(showTest) test_image.draw(camWidth*SCALE +100, 0);
	if(showFaces) rec.drawFaces(0, ofGetHeight()*0.8, ofGetWidth());
	if(showEigens) rec.drawEigens(0, ofGetHeight()*0.9, ofGetWidth());
	*/
	int person=-1;
	int w = 100, h = 12;
	
	ofPushStyle();
	ofPushMatrix();
	ofTranslate(5, 10);
	int n = classifier.size();
	if(n>0){
		primaryExpression = classifier.getPrimaryExpression();
		if(expressionType.compare(classifier.getDescription(primaryExpression))!=0){\
			expressionType=classifier.getDescription(primaryExpression);
			cout << "expressionType" <<expressionType <<"\n";
			if(music.getIsPlaying()) music.stop();
			if(expressionType.compare("angry")==0){
				serial.writeString("l21");
				printf("angry l21");
				//serial.writeString("l10");
				//serial.writeString("l40");
				//if (stablePersonName.compare("luhan") == 0) serial.writeString("t23");
				//else serial.writeString("t24");
			}else if(expressionType.compare("happy")==0){
				if (stablePersonName.compare("kennis") == 0) serial.writeString("l41");
				else serial.writeString("l01");
				//serial.writeString("l10");
				//serial.writeString("l20");
				//serial.writeString("f0");
				//if (stablePersonName.compare("amy") == 0) serial.writeString("t26");
			//	else serial.writeString("t25");
			}else if(expressionType.compare("sad")==0){
				serial.writeString("l11");
				//serial.writeString("l31");
				//serial.writeString("l00");
				//serial.writeString("l20");
				//serial.writeString("l40");	
				//serial.writeString("f0");
				if (stablePersonName.compare("amy") == 0) serial.writeString("t28");
				else serial.writeString("t27");
			}
			else if (expressionType.compare("neutral") == 0){
				serial.writeString("l00");
				//serial.writeString("f0");
			}

			music.loadSound("music/"+stablePersonName+"-"+expressionType+".mp3");
			music.play();

		}
		for(int i = 0; i < n; i++){
			ofSetColor(i == primaryExpression ? ofColor::red : ofColor::black);
			ofRect(0, 0, w * classifier.getProbability(i) + .5, h);
			ofSetColor(255);
			ofDrawBitmapString(classifier.getDescription(i), 5, 9);
			ofTranslate(0, h + 5);
		}
	}

	ofPopMatrix();
	ofPopStyle();
	
	ofDrawBitmapString(ofToString((int) ofGetFrameRate()), ofGetWidth() - 20, ofGetHeight() - 10);
	drawHighlightString(
		string() +
		"r - reset\n" +
		"e - add expression\n" +
		"a - add sample\n" +
		"s - save expressions\n"
		"l - load expressions",
		14, ofGetHeight() - 7 * 12);

	std::ostringstream fr;
    std::ostringstream o;
	for(int i = 0; i < finder.blobs.size(); i++) {
		ofRectangle cur = finder.blobs[i].boundingRect;
        /*cur.x*=TEST_DIV;
        cur.y*=TEST_DIV;
        cur.width*=TEST_DIV;
        cur.height*=TEST_DIV;
		*/
        int tx=cur.x;
        int ty=cur.y;
        int tw=cur.width;
        int th=cur.height;

        unsigned char *img_px = img.getPixels();

		unsigned char *temp = new unsigned char[tw*th*3];
		for (int x=0; x<tw; x++)
            for (int y=0; y<th; y++) {
                temp[(x+y*tw)*3] = img_px[((x+tx)+(y+ty)*camWidth)*3];
                temp[(x+y*tw)*3+1] = img_px[((x+tx)+(y+ty)*camWidth)*3+1];
                temp[(x+y*tw)*3+2] = img_px[((x+tx)+(y+ty)*camWidth)*3+2];
            }
        face.setFromPixels(temp, cur.width, cur.height, OF_IMAGE_COLOR);
        delete temp;

        face.resize(PCA_WIDTH, PCA_HEIGHT);
        //face.update();
        color = face.getPixels();
        gray = color;

        unsigned char *pixels = gray.getPixels();
        for(int x=0; x<PCA_WIDTH; x++)
            for(int y=0; y<PCA_HEIGHT; y++)
                if(mask.getPixels()[x+y*PCA_HEIGHT]<=0)
                    pixels[x+y*PCA_HEIGHT]=128;
        gray = pixels;

		person=rec.recognize(color, &currentPersonName);
		if(person==-1){
			serial.writeString("f0");
			printf("fan off");
		}else{
			if (!messageSent){
				serial.writeString("f1");
				printf("fan on");
				messageSent = true;
			}

		}
		if(personIndex!=person){
			printf("currentPersonName %s\n", currentPersonName.c_str());
			countAbnormalPerson++;
			if(countAbnormalPerson>=3){
				countAbnormalPerson=0;
				personIndex=person;
				stablePersonName=currentPersonName;
				printf("stablePersonName %s\n", stablePersonName.c_str());
			}
		}
	}
}

void testApp::keyPressed(int key) {
	if(key == 'r') {
		tracker.reset();
		classifier.reset();
	}
	if(key == 'e') {
		classifier.addExpression();
	}
	if(key == 'a') {
		classifier.addSample(tracker);
	}
	if(key == 's') {
		classifier.save("expressions");
	}
	if(key == 'l') {
		printf("load classifier");
		classifier.load("expressions");
	}
	 if((key == 'i') || (key == 'I'))
        showEigens = (showEigens == false);

    if((key == 'f') || (key == 'F'))
        showFaces = (showFaces == false);


    if((key == 'q') || (key == 'Q'))
        showLeastSq = (showLeastSq == false);
	
	if (key == 'n'){	
        rec.addPerson("name");
    }
    if (key == 't'){	
        rec.startTraining();
		showTest = (showTest == false);
    }
    if (key == 'q'){	
        rec.stopTraining();        
    }
	if (key == 'z'){
		rec.saveNextFaces = TRUE;
	}
}

void testApp::calcFaceSprites() {
  /*  if(!rec.isTrained()) return;

    for (int i=0; i<rec.numPeople(); i++) {
        ofImage masked;

        unsigned char* pixels = rec.getPersonPixels(i);
        unsigned char* rgba_pixels = new unsigned char[4*PCA_WIDTH*PCA_HEIGHT];
        for(int x=0; x<PCA_WIDTH; x++)
            for(int y=0; y<PCA_HEIGHT; y++) {
                rgba_pixels[(x+(y*PCA_WIDTH))*4] = pixels[(x+(y*PCA_WIDTH))*3];
                rgba_pixels[(x+(y*PCA_WIDTH))*4+1] = pixels[(x+(y*PCA_WIDTH))*3+1];
                rgba_pixels[(x+(y*PCA_WIDTH))*4+2] = pixels[(x+(y*PCA_WIDTH))*3+2];
                rgba_pixels[(x+(y*PCA_WIDTH))*4+3] = mask_pixels[x+y*PCA_WIDTH];
            } 
        masked.setFromPixels(rgba_pixels, PCA_WIDTH, PCA_HEIGHT, OF_IMAGE_COLOR_ALPHA);
        faces.push_back(masked);
        delete rgba_pixels;
    };
	*/
}