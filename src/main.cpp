#include "testApp.h"
#include "ofAppGlutWindow.h"

int main() {

	ofAppGlutWindow window;
	ofSetupOpenGL(&window, 1900, 1100, OF_WINDOW);
	ofRunApp(new testApp());
}
