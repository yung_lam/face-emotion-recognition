#pragma once

#include "ofMain.h"
#include "ofxFaceTracker.h"
#include "ofxCvHaarFinder.h"
#include "ofxCvFaceRec.h"
#include "ofxSimpleSerial.h"
#include "ofxUI.h"

#define SCALE 1
#define TEST_DIV 2
#define CAM_WIDTH 320
#define CAM_HEIGHT 240
struct countNameExist {
	int  count;
	bool isVisit;
	bool isStable;
	string name;
	countNameExist(int c, string n){
		count=c;
		name=n;
		isVisit=true;
		isStable=false;
	}
};


class testApp : public ofBaseApp {
public:
	void setup();
	void update();
	void draw();
	void keyPressed(int key);
	void guiEvent(ofxUIEventArgs &e);
	void exit();
	ofImage img;
	ofImage test_image;
	ofImage bgImage;
	ofImage mask;
	unsigned char *mask_pixels;
	string newPersonName;
	ofxCvHaarFinder finder;
    ofxCvFaceRec rec;	

	ofVideoGrabber cam;
	ofxFaceTracker tracker;
	ExpressionClassifier classifier;
	string expressionType;
	int countAbnormalExpression;
	ofTexture			videoTexture;
	//int 				camWidth;
	//int 				camHeight;
	int	countBeingStable;
	int personIndex, primaryExpression;
	//const static int camPosXOffset = 105, camPosYOffset = 220;
	ofImage face;
    ofxCvColorImage color;
    ofxCvGrayscaleImage gray;
	ofSoundStream soundStream;
    vector <ofImage> faces;
	ofSoundPlayer  music;
	ofxSimpleSerial serial;
	string	arduinoCmd;
	vector<string>	currentPersonName;
	vector<countNameExist> stablePersonList;
	string stablePersonName;
	bool messageSent;
	ofxUICanvas *gui;
	void drawDetectedPerson();
	void setupGUI();
	int livingTemp, EnterTemp, WorkTemp;
	void setToggleOnOff(string id, bool val, string serialCmd);
private:
    // vars to toggle onscreen display
    bool showEigens;
    bool showFaces;
    bool showExtracted;
    bool showTest;
    bool showLeastSq;
    bool bgSubtract;
    bool showClock;
	bool isRecognizing;
	bool isExpressionTracking;
	ofxUILabel *confirmPersonLabel;
	ofxUILabel *expressionLabel;
	ofxUILabel *livingLabel;
	ofxUILabel *EnterLabel;
	ofxUILabel *WorkLabel;
	ofxUIToggle *workToggle;
	ofxUIToggle *enterToggle;
	ofxUIToggle *livingToggle;
	ofxUITextInput *NameInput;
	string roomStr;
};
#pragma once