#include "testApp.h"

using namespace ofxCv;
using namespace cv;

void testApp::setup() {
	ofSetLogLevel(OF_LOG_FATAL_ERROR);
	ofSetVerticalSync(true);
	cam.setVerbose(true);
	camWidth=640;
	camHeight=480;
	cam.initGrabber(camWidth, camHeight);
	videoMirror = new unsigned char[camWidth*camHeight*3];
	mirrorTexture.allocate(camWidth, camHeight, GL_RGB);
	tracker.setup();
	tracker.setRescale(.5);
	
	finder.setup("haarcascade_frontalface_default.xml");
	//finder.findHaarObjects(img);
	mask.loadImage("mask.png");
    mask.resize(PCA_WIDTH, PCA_HEIGHT);
    mask_pixels = mask.getPixels();
	rec.learn();
    gray.allocate(PCA_WIDTH, PCA_HEIGHT);
    color.allocate(PCA_WIDTH, PCA_HEIGHT);
	// PRECALCULATE TRANSLUCENT FACE SPRITES
	rec.cascade=finder.cascade;
    showEigens = showFaces = showExtracted = showTest = showLeastSq = showClock = bgSubtract=false;
	isRecognizing=false;
	expressionType="neutral";
	music.setMultiPlay(false);
	music.setVolume(0.8f);
    ofEnableAlphaBlending();
	arduinoCmd = ""; currentPersonName = "";
	serial.setup(1, 9600);
	//serial.startContinuousRead(false);
	ofSetFrameRate(30);
	countAbnormalPerson=0;
	stablePersonName="";
	messageSent = false;
	gui = new ofxUICanvas(0,0,1024,768);	
	gui->addWidget(new ofxUIToggle(650,20,30,30,false,"Recognize")); 
	gui->addWidget(new ofxUIButton(650,70,30,30,false,"New User")); 
	gui->addWidget(new ofxUIButton(650,400,30,30,false,"Emotion Recognition")); 
	//gui->addWidgetDown(new ofxUITextInput());
	ofAddListener(gui->newGUIEvent, this, &testApp::guiEvent); 
	gui->loadSettings("GUI/guiSettings.xml"); 

/* */
}

void testApp::update() {
	cam.update();
	if(cam.isFrameNew()) {
		if(tracker.update(toCv(cam))) {
			classifier.classify(tracker);
		}
		 unsigned char * pixels = cam.getPixels();
	
		color.setFromPixels(pixels,camWidth, camHeight);

	}
}

void testApp::draw() {
	ofSetColor(255);
	color.draw(0, 0);
	/* tracker.draw();
	   // display other items
    if(showTest) test_image.draw(camWidth*SCALE +100, 0);
	if(showFaces) rec.drawFaces(0, ofGetHeight()*0.8, ofGetWidth());
	if(showEigens) rec.drawEigens(0, ofGetHeight()*0.9, ofGetWidth());
	*/
	int person=-1;
	int w = 100, h = 12;
	
	ofPushStyle();
	ofPushMatrix();
	ofTranslate(5, 10);
	int n = classifier.size();
	if(n>0){
		primaryExpression = classifier.getPrimaryExpression();
		if(expressionType.compare(classifier.getDescription(primaryExpression))!=0){\
			expressionType=classifier.getDescription(primaryExpression);
			cout << "expressionType" <<expressionType <<"\n";
			if(music.getIsPlaying()) music.stop();
			if(expressionType.compare("angry")==0){
				serial.writeString("l21");
				printf("angry l21");
			}else if(expressionType.compare("happy")==0){
				if (stablePersonName.compare("kennis") == 0) serial.writeString("l41");
				else serial.writeString("l01");
			}else if(expressionType.compare("sad")==0){
				serial.writeString("l11");
				if (stablePersonName.compare("amy") == 0) serial.writeString("t28");
				else serial.writeString("t27");
			}
			else if (expressionType.compare("neutral") == 0){
				serial.writeString("l00");
			}

			music.loadSound("music/"+stablePersonName+"-"+expressionType+".mp3");
			music.play();

		}
		for(int i = 0; i < n; i++){
			ofSetColor(i == primaryExpression ? ofColor::red : ofColor::black);
			ofRect(0, 0, w * classifier.getProbability(i) + .5, h);
			ofSetColor(255);
			ofDrawBitmapString(classifier.getDescription(i), 5, 9);
			ofTranslate(0, h + 5);
		}
	}

	ofPopMatrix();
	ofPopStyle();
	
	
	if(isRecognizing){
			person=rec.recognize(color, &currentPersonName);
			if(person==-1){
				serial.writeString("f0");
				printf("fan off");
			}else{
				if (!messageSent){
					serial.writeString("f1");
					printf("fan on");
					messageSent = true;
				}

			}

			if(personIndex!=person){
				printf("currentPersonName %s\n", currentPersonName.c_str());
				countAbnormalPerson++;
				if(countAbnormalPerson>=3){
					countAbnormalPerson=0;
					personIndex=person;
					stablePersonName=currentPersonName;
					
					printf("stablePersonName %s\n", stablePersonName.c_str());
				}
			}
		//}
	}
	drawDetectedPerson();
}

void testApp::keyPressed(int key) {
	/*
	if(key == 'r') {
		tracker.reset();
		classifier.reset();
	}
	if(key == 'e') {
		classifier.addExpression();
	}
	if(key == 'a') {
		classifier.addSample(tracker);
	}
	if(key == 's') {
		classifier.save("expressions");
	}
	if(key == 'l') {
		printf("load classifier");
		classifier.load("expressions");
	}
	 if((key == 'i') || (key == 'I'))
        showEigens = (showEigens == false);

    if((key == 'f') || (key == 'F'))
        showFaces = (showFaces == false);


    if((key == 'q') || (key == 'Q'))
        showLeastSq = (showLeastSq == false);
	
	if (key == 'n'){	
        //rec.addPerson();
    }
    if (key == 't'){	
        rec.startTraining();
		//showTest = (showTest == false);
    }
    if (key == 'q'){	
        rec.stopTraining();        
    }
	if (key == 'z'){
		rec.saveNextFaces = TRUE;
	}
	*/
}

void testApp::exit()
{
    gui->saveSettings("GUI/guiSettings.xml");     
    delete gui; 
}
void testApp::guiEvent(ofxUIEventArgs &e)
{
    if(e.widget->getName() == "BACKGROUND VALUE")
    {
        ofxUISlider *slider = (ofxUISlider *) e.widget;    
        ofBackground(slider->getScaledValue());
    }
    else if(e.widget->getName() == "FULLSCREEN")
    {
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        ofSetFullscreen(toggle->getValue()); 
    }else if(e.widget->getName() == "Recognize"){
		ofxUIToggle *btn = (ofxUIToggle *) e.widget;    
		isRecognizing = btn->getValue();
	}else if(e.widget->getName()=="New User"){
		ofxUIButton *btn = (ofxUIButton *) e.widget;  
		if(btn->getValue()==true){
			gui->addWidget(new ofxUITextInput(650, 110, 180, "NameInput", "NewUserName",OFX_UI_FONT_SMALL)); 
			gui->addWidget(new ofxUIButton(650,140,30,30,false,"Confirm")); 
			//gui->getw
		}
	}else if(e.widget->getName()=="Confirm"){
		ofxUIButton *btn = (ofxUIButton *) e.widget;  
		if(btn->getValue()==true){
			//ofxUITextInput *input=(ofxUITextInput *)gui->getWidget("NameInput");
			//string userName=input->getTextString();
			printf("new userName %s\n", newPersonName.c_str());
			rec.addPerson(newPersonName);
			//rec.startTraining();
			isRecognizing=true;
			gui->addWidget(new ofxUIButton(650,140,30,30,false,"Add Face")); 
			gui->addWidget(new ofxUIButton(750,140,30,30,false,"Finish Face Adding")); 
			//btn->setVisible(false);
			//btn->removeWidget();
			gui->removeWidget(btn);
			//gui->getw
		}
		//gui->removeWidget(btn);
	}else if(e.widget->getName()=="NameInput"){
		ofxUITextInput *input=(ofxUITextInput *)e.widget;
		newPersonName=input->getTextString();
		
	}else if(e.widget->getName()=="Add Face"){
		ofxUIButton *btn = (ofxUIButton *) e.widget;  
		if(btn->getValue()==true){
			rec.saveNextFaces = TRUE;
		}
	}else if(e.widget->getName()=="Finish Face Adding"){
		ofxUIButton *btn = (ofxUIButton *) e.widget;  
		if(btn->getValue()==true){
			ofxUIButton *btn=(ofxUIButton *)gui->getWidget("Add Face");
			gui->removeWidget(btn);
			ofxUIButton *btnF=(ofxUIButton *)gui->getWidget("Finish Face Adding");
			gui->removeWidget(btnF);
			ofxUITextInput *input=(ofxUITextInput *)gui->getWidget("NameInput");
			//gui->removeWidget(input);
			rec.startTraining();
		}
	}else if(e.widget->getName()=="Emotion Recognition"){
		ofxUIButton *btn = (ofxUIButton *) e.widget;  
		if(btn->getValue()==true){
			printf("load classifier");
			classifier.load("expressions");
		}
	}
}
 
void testApp::drawDetectedPerson()
{
	ofDrawBitmapString(ofToString((int) ofGetFrameRate()), ofGetWidth() - 20, ofGetHeight() - 10);
	drawHighlightString(
		string() +
		"r - reset\n" +
		"e - add expression\n" +
		"a - add sample\n" +
		"s - save expressions\n"
		"l - load expressions",
		14, ofGetHeight() - 7 * 12);
	drawHighlightString(
	"Detected Person:" + stablePersonName,
	20, 520);
	for(int i=0;i<rec.nPersons;i++){
	rec.personNames;
	}
}