﻿#include "ofxCvFaceRec.h"

ofxCvFaceRec::ofxCvFaceRec() {
    nTrainFaces               = 0; // the number of training images
    nEigens                   = 0; // the number of eigenvalues
    projectedTrainFaceMat = 0; // projected training faces
    faceImgArr        = 0; // array of face images
    eigenVectArr      = 0; // eigenvectors
    pAvgTrainImg       = 0; // the average image
    eigenValMat        = 0   ; // eigenvalues
    personNumTruthMat = 0; // array of person numbers
    trained = false;
	state = -1;
	faceWidth = faceHeight = 150;
}

ofxCvFaceRec::~ofxCvFaceRec() {
}

void ofxCvFaceRec::learn() {
    int i, offset;	
    // load training data
    nTrainFaces = loadFaceImgArray("train.txt");
    if( nTrainFaces < 2 )
    {
    printf("Need 2 or more training faces\n"
            "Input file contains only %d\n", nTrainFaces);
    return;
    }

    // do PCA on the training faces
    doPCA();

    // project the training images onto the PCA subspace
    projectedTrainFaceMat = cvCreateMat( nTrainFaces, nEigens, CV_32FC1 );
    offset = projectedTrainFaceMat->step / sizeof(float);
    for(i=0; i<nTrainFaces; i++)
    {
        //int offset = i * nEigens;
        cvEigenDecomposite(
            faceImgArr[i],
            nEigens,
            eigenVectArr,
            0, 0,
            pAvgTrainImg,
            projectedTrainFaceMat->data.fl + i*nEigens);
            //projectedTrainFaceMat->data.fl + i*offset);
    }
    // store the recognition data as an xml file
    storeTrainingData();
	 printf("Finish learning Input file contains only %d nTrainFaces\n", nTrainFaces);
    trained=false;
}

void ofxCvFaceRec::mask(ofxCvGrayscaleImage img) {

};

int ofxCvFaceRec::recognize(ofxCvColorImage colorImg, string *personName) {
	ofxCvGrayscaleImage img;
	img = colorImg;
    int i, nearest, truth, iNearest;
	float confidence;
    //nTestFaces  = 0;         // the number of test images

    float * projectedTestFace = 0;
    char Buf[50]; char cstr[256];
	IplImage *faceImg, *greyImg, *sizedImg, *equalizedImg;
    // load the saved training data
    if(!trained) {
        if( loadTrainingData( &trainPersonNumMat ) ) trained=true;
    };

	if (state == 1) {
		
		//strcpy(newPersonName, "newPerson");
		//fgets(newPersonName, sizeof(newPersonName)-1, stdin);
        // Remove 1 or 2 newline characters if they were appended (eg: Linux).
        i = strlen(newPersonName);
		printf("strlen(newPersonName) %d", i);
        if (i > 0 && (newPersonName[i-1] == 10 || newPersonName[i-1] == 13)) {
            newPersonName[i-1] = 0;
            i--;
        }
        if (i > 0 && (newPersonName[i-1] == 10 || newPersonName[i-1] == 13)) {
            newPersonName[i-1] = 0;
            i--;
        }
            
        if (i > 0) {
            printf("Collecting all images until you hit 't', to start Training the images as '%s' ...\n", newPersonName);
            newPersonFaces = 0;	// restart training a new person
            saveNextFaces = TRUE;
            state = 2;
        } else {
            printf("Did not get a valid name from you, so will ignore it. Hit 'n' to retry.\n");
        }
	} 
	 if (state == 3){
            saveNextFaces = FALSE;	// stop saving next faces.
            // Store the saved data into the training file.
            printf("Storing the training data for new person '%s'.\n", newPersonName);
            // Append the new person to the end of the training data.
            FILE *trainFile = fopen(ofToDataPath("train.txt").c_str(), "a");
            for (i=0; i<newPersonFaces; i++) {
                _snprintf(cstr, sizeof(cstr)-1, "%d_%s%d.pgm", nPersons+1, newPersonName, i);
				printf("Storing as '%s'.\n", cstr);
                fprintf(trainFile, "%d %s %s\n", nPersons+1, newPersonName, cstr);
            }
            fclose(trainFile);
            // Now there is one more person in the database, ready for retraining.
            //nPersons++;
            // Re-initialize the local data.
            projectedTestFace = 0;
            newPersonFaces = 0;
            
            // Retrain from the new database without shutting down.
            // Depending on the number of images in the training set and number of people, it might take 30 seconds or so.
            //cvFree( &trainPersonNumMat );	// Free the previous data before getting new data
            trainPersonNumMat = retrainOnline();
            // Project the test images onto the PCA subspace
            cvFree(&projectedTestFace);	// Free the previous data before getting new data
            projectedTestFace = (float *)cvAlloc( nEigens*sizeof(float) );
            
            printf("Recognizing person in the camera ...\n");
            state = 0;
            
        }

    // project the test images onto the PCA subspace
    projectedTestFace = (float *)cvAlloc( nEigens*sizeof(float) );

	IplImage* cameraImg = img.getCvImage(); 
	// Make sure the image is greyscale, since the Eigenfaces is only done on greyscale image.*/
	greyImg = convertImageToGreyscale(cameraImg);
	// Perform face detection on the input image, using the given Haar cascade classifier.
	CvRect faceRect = detectFaceInImage(cameraImg, cascade );
	if (faceRect.width > 0) {
        faceImg = cropImage(greyImg, faceRect);	// Get the detected face image.
		// Make sure the image is the same dimensions as the training images.
		sizedImg = resizeImage(faceImg, faceWidth, faceHeight);
		// Give the image a standard brightness and contrast, in case it was too dark or low contrast.
		equalizedImg = cvCreateImage(cvGetSize(sizedImg), 8, 1);	// Create an empty greyscale image
		cvEqualizeHist(sizedImg, equalizedImg);
		if (!equalizedImg) {
			printf("ERROR in recognizeFromCam(): Don't have input image!\n");
			exit(1);
		}
		// If the face rec database has been loaded, then try to recognize the person currently detected.
		if (nEigens > 0) {
			// project the test image onto the PCA subspace
			cvEigenDecomposite(equalizedImg, nEigens, eigenVectArr, 0, 0, pAvgTrainImg, projectedTestFace);
                
			// Check which person it is most likely to be.
			iNearest = findNearestNeighbor(projectedTestFace, &confidence);
			nearest  = trainPersonNumMat->data.i[iNearest];
			*personName = personNames[nearest-1];
            printf("Confidence = %f iNearest = %d nearest = %d personNames %s\n", confidence, iNearest,nearest,  personNames[nearest-1].c_str());
            //cout << "personNames" << personNames[nearest-1].c_str();
            // name_x = faceRect.width;
            // name_y = faceRect.height;
       
		}//endif nEigens
		if (state == 2) {
			if(saveNextFaces){ //need to press 'z' key to open 
				_snprintf(cstr, sizeof(cstr)-1, ofToDataPath("faces\\%d_%s%d.pgm").c_str(), nPersons+1, newPersonName, newPersonFaces);
				printf("Storing the current face of '%s' into image '%s'.\n", newPersonName, cstr);
				cvSaveImage(cstr, equalizedImg, NULL);
				/*uchar* imPixels=new uchar[faceWidth*(newPersonFaces+1)* faceHeight];  
				if(newPersonFaces>0){
					IplImage* old = shownImage.getCvImage();
					for( int y=0; y<faceHeight; y++ ) {  
						uchar* ptrOld = (uchar*) (old->imageData + y * old->widthStep);
						for( int x=0; x<old->width; x++ ) {  
							imPixels[x+y*old->width]=*ptrOld;  
							ptrOld++;  
						}  
						uchar* ptr = (uchar*) (	equalizedImg->imageData + y * equalizedImg->widthStep);  
						for( int x=old->width; x<(old->width+equalizedImg->width); x++ ) {  
							imPixels[x+y*equalizedImg->width]=*ptr;  
							ptr++;  
						}  
					}  
				}else{
					for( int y=0; y<faceHeight; y++ ) {  
						uchar* ptr = (uchar*) (	equalizedImg->imageData + y * equalizedImg->widthStep);  
						for( int x=0; x<equalizedImg->width; x++ ) {  
							imPixels[x+y*equalizedImg->width]=*ptr;  
							ptr++;  
						}  
					}
				}
				shownImage.allocate(faceWidth*(newPersonFaces+1)*, faceHeight);  
				shownImage.setFromPixels(imPixels,faceWidth*(newPersonFaces+1), faceHeight);
				*/
				uchar* imPixels=new uchar[faceWidth*faceHeight];  
				
				for( int y=0; y<faceHeight; y++ ) {  
					uchar* ptr = (uchar*) (	equalizedImg->imageData + y * equalizedImg->widthStep);  
					for( int x=0; x<equalizedImg->width; x++ ) {  
						imPixels[x+y*equalizedImg->width]=*ptr;  
						ptr++;  
					}  
				}
				shownImage.allocate(faceWidth, faceHeight);  
				shownImage.setFromPixels(imPixels,faceWidth, faceHeight);
				delete []imPixels;  
				newPersonFaces++;
				saveNextFaces=FALSE;
			}
		}
		shownImage.draw(660,200);
	}
	if(confidence>0.2 || confidence<-0.2) 
		return nearest-1;
	else
		return -1;
}
void ofxCvFaceRec::doPCA() {
    int i;
	CvTermCriteria calcLimit;
	CvSize faceImgSize;

	// set the number of eigenvalues to use
	nEigens = nTrainFaces-1;

	// allocate the eigenvector images
	faceImgSize.width  = faceImgArr[0]->width;
	faceImgSize.height = faceImgArr[0]->height;
	eigenVectArr = (IplImage**)cvAlloc(sizeof(IplImage*) * nEigens);
	for(i=0; i<nEigens; i++)
		eigenVectArr[i] = cvCreateImage(faceImgSize, IPL_DEPTH_32F, 1);

	// allocate the eigenvalue array
	eigenValMat = cvCreateMat( 1, nEigens, CV_32FC1 );

	// allocate the averaged image
	pAvgTrainImg = cvCreateImage(faceImgSize, IPL_DEPTH_32F, 1);

	// set the PCA termination criterion
	calcLimit = cvTermCriteria( CV_TERMCRIT_ITER, nEigens, 1);

	// compute average image, eigenvalues, and eigenvectors
	cvCalcEigenObjects(
		nTrainFaces,
		(void*)faceImgArr,
		(void*)eigenVectArr,
		CV_EIGOBJ_NO_CALLBACK,
		0,
		0,
		&calcLimit,
		pAvgTrainImg,
		eigenValMat->data.fl);

    ofxCvFloatImage eig;

    for(i=0; i<nEigens; i++) {
        // add eigenVectArr[i] to eigens
        //eig.setFromPixels(eigenVectArr[i]->imageData, eigenVectArr[i]->width, eigenVectArr[i]->height);
        eig.allocate(faceImgSize.width, faceImgSize.height);

        eig=eigenVectArr[i];
        eig.convertToRange(0., 255.);
        eigens.push_back(eig);
        eig.clear();
    };

	cvNormalize(eigenValMat, eigenValMat, 1, 0, CV_L1, 0);
}

void ofxCvFaceRec::storeTrainingData() {
    CvFileStorage * fileStorage;
	int i;
	fileStorage = cvOpenFileStorage(ofToDataPath("facedata.xml").c_str(), 0, CV_STORAGE_WRITE );
	// Store the person names
	cvWriteInt( fileStorage, "nPersons", nPersons );
	for (i=0; i<nPersons; i++) {
		char varname[200];
		_snprintf( varname, sizeof(varname)-1, "personName_%d", (i+1) );
		cvWriteString(fileStorage, varname, personNames[i].c_str(), 0);
	}
	// store all the data
	cvWriteInt( fileStorage, "nEigens", nEigens );
	cvWriteInt( fileStorage, "nTrainFaces", nTrainFaces );
	cvWrite(fileStorage, "trainPersonNumMat", personNumTruthMat, cvAttrList(0,0));
	cvWrite(fileStorage, "eigenValMat", eigenValMat, cvAttrList(0,0));
	cvWrite(fileStorage, "projectedTrainFaceMat", projectedTrainFaceMat, cvAttrList(0,0));
	cvWrite(fileStorage, "avgTrainImg", pAvgTrainImg, cvAttrList(0,0));
	for(i=0; i<nEigens; i++){
		char varname[200];
		_snprintf( varname, sizeof(varname)-1, "eigenVect_%d", i );
		cvWrite(fileStorage, varname, eigenVectArr[i], cvAttrList(0,0));
	}
	// release the file-storage interface
	cvReleaseFileStorage( &fileStorage );
}

int ofxCvFaceRec::loadTrainingData(CvMat ** pTrainPersonNumMat) {
	CvFileStorage * fileStorage;
	int i;
	// create a file-storage interface
	fileStorage = cvOpenFileStorage(ofToDataPath("facedata.xml").c_str() , 0, CV_STORAGE_READ );
	if( !fileStorage )
	{
		fprintf(stderr, "Can't open facedata.xml\n");
		return 0;
	}
	personNames.clear();
	nPersons = cvReadIntByName( fileStorage, 0, "nPersons", 0 );
	if (nPersons == 0) {
		printf("No people found in the training database 'facedata.xml'.\n");
		return 0;
	}
	// Load each person's name.
	for (i=0; i<nPersons; i++) {
		string sPersonName;
		char varname[200];
		_snprintf( varname, sizeof(varname)-1, "personName_%d", (i+1) );
		sPersonName = cvReadStringByName(fileStorage, 0, varname );
		personNames.push_back( sPersonName );
	}

	nEigens = cvReadIntByName(fileStorage, 0, "nEigens", 0);
	nTrainFaces = cvReadIntByName(fileStorage, 0, "nTrainFaces", 0);
	*pTrainPersonNumMat = (CvMat *)cvReadByName(fileStorage, 0, "trainPersonNumMat", 0);
	eigenValMat  = (CvMat *)cvReadByName(fileStorage, 0, "eigenValMat", 0);
	projectedTrainFaceMat = (CvMat *)cvReadByName(fileStorage, 0, "projectedTrainFaceMat", 0);
	pAvgTrainImg = (IplImage *)cvReadByName(fileStorage, 0, "avgTrainImg", 0);
	eigenVectArr = (IplImage **)cvAlloc(nTrainFaces*sizeof(IplImage *));
	for(i=0; i<nEigens; i++)
	{
		char varname[200];
		//sprintf( varname, "eigenVect_%d", i );
		_snprintf( varname, sizeof(varname)-1, "eigenVect_%d", i );
		eigenVectArr[i] = (IplImage *)cvReadByName(fileStorage, 0, varname, 0);
	}
	cvReleaseFileStorage( &fileStorage );
	printf("Training data loaded (%d training images of %d people):\n", nTrainFaces, nPersons);
	printf("People: ");
	if (nPersons > 0)
		printf("<%s>", personNames[0].c_str());
	for (i=1; i<nPersons; i++) {
		printf(", <%s>", personNames[i].c_str());
	}
	printf(".\n");
	return 1;
}
// Find the most likely person based on a detection. Returns the index, and stores the confidence value into pConfidence.
int ofxCvFaceRec::findNearestNeighbor(float *projectedTestFace, float *pConfidence) {
      //double leastDistSq = 1e12;
    char Buf[100];
    //Out->clear();
    leastDistSq = DBL_MAX;
    int i, iTrain, iNearest = 0;

    for(iTrain=0; iTrain<nTrainFaces; iTrain++)
    {
      double distSq=0;
      for(i=0; i<nEigens; i++)
      {
        float d_i =	projectedTestFace[i] -
            projectedTrainFaceMat->data.fl[iTrain*nEigens + i];

            distSq += d_i*d_i; // Euclidean

      }
      //sprintf(Buf,"%03d  ->  %f",iTrain+1,distSq);
      //Out->add(Buf);

      if(distSq < leastDistSq){
         leastDistSq = distSq;
         iNearest = iTrain;
      }
    }
	// Return the confidence level based on the Euclidean distance,
	// so that similar images should give a confidence between 0.5 to 1.0,
	// and very different images should give a confidence between 0.0 to 0.5.
	*pConfidence = 1.0f - sqrt( leastDistSq / (float)(nTrainFaces * nEigens) ) / 255.0f;
    //printf("leastDistSq: %03d -> %f\n", iNearest, leastDistSq);

    return iNearest;
}

int ofxCvFaceRec::loadFaceImgArray(char * filename) {
    FILE * imgListFile = 0;
    char imgFilename[512];
    char Buf[512];
    int iFace, nFaces=0;
	ofImage img;
    string fileName = ofToDataPath(filename);
    // open the input file
    if( !(imgListFile = fopen(fileName.c_str(), "r")) )
    {
        fprintf(stderr, "Can\'t open file %s\n", fileName.c_str());
        return 0;
    } 
    // count the number of faces
    while( fgets(imgFilename, 512, imgListFile) ) {
        ++nFaces;
    };
    rewind(imgListFile);
    
    // allocate the face-image array and person number matrix
    faceImgArr        = (IplImage **)cvAlloc( nFaces*sizeof(IplImage *) );
    personNumTruthMat = cvCreateMat( 1, nFaces, CV_32SC1 );
	personNames.clear();	// Make sure it starts as empty.
	nPersons = 0;
    if(!faces.empty()) faces.clear();

    // store the face images in an array
    for(iFace=0; iFace<nFaces; iFace++)
    {
		char personName[256];
		string sPersonName;
		int personNumber;

        //fgets(imgFilename, 512, imgListFile);
		// read person number (beginning with 1), their name and the image filename.
		fscanf(imgListFile, "%d %s %s", &personNumber, personName, imgFilename);
		sPersonName = personName;
        printf(" %s filename: %s\n", personName, imgFilename);

		// Check if a new person is being loaded.
		if (personNumber > nPersons) {
			// Allocate memory for the extra person (or possibly multiple), using this new person's name.
			for (int i=nPersons; i < personNumber; i++) {
				personNames.push_back( sPersonName );
			}
			nPersons = personNumber;
			printf("Got new person <%s> -> nPersons = %d [%d]\n", sPersonName.c_str(), nPersons, personNames.size());
		}
		// Keep the data
		personNumTruthMat->data.i[iFace] = personNumber;

        string temp = imgFilename;
        temp = "faces/"+temp;

        string inImage = ofToDataPath(temp);
        //inImage = inImage.substr(0, inImage.length()-1);
        //strcpy(imgFilename, ofToDataPath(files[iFace]).c_str());
       // *(personNumTruthMat->data.i+iFace)= iFace+1;
        if (img.loadImage(inImage) == false) {
            printf("Can\'t load image from %s\n", inImage.c_str());
            return 0;
        }

        img.resize(PCA_WIDTH, PCA_HEIGHT);
        img.update();

        ofxCvGrayscaleImage gray;
        ofxCvColorImage color;

        switch(img.type) {
            case OF_IMAGE_GRAYSCALE:
                gray.allocate(img.width, img.height);
                gray = img.getPixels();//color;
                break;
            case OF_IMAGE_COLOR:
            case OF_IMAGE_COLOR_ALPHA:
                color.allocate(img.width, img.height);
                color = img.getPixels();
                gray.allocate(img.width, img.height);
                gray = color;
                break;
            case OF_IMAGE_UNDEFINED:
                printf("Image is of unknown type, %s\n", imgFilename);
                return 0;
        };
        //img.draw(200+iFace*100, 200);//, 50, 50);
        //color.draw(200+iFace*100, 300);//, 50, 50);
        //gray.draw(200+iFace*100, 400);//, 50, 50);

        // add gray image to array of training images
        //gray.contrastStretch();
        faces.push_back(gray);
        color_faces.push_back(color);

        IplImage *im8 = gray.getCvImage();
        faceImgArr[iFace] = cvCloneImage(im8);
    }

    fclose(imgListFile);

    return nFaces;
}

void ofxCvFaceRec::draw(int x, int y)
{
    drawFaces(x, y);
    drawEigens(x, y+faces[0].height+25);
}

void ofxCvFaceRec::drawFaces(int x, int y)
{
    int i;

	for(i = 0; i < faces.size(); i++)
        faces[i].draw((x+i*faces[i].width), y);
}

void ofxCvFaceRec::drawFaces(int x, int y, int width)
{
    int i;

	for(i = 0; i < faces.size(); i++)
        faces[i].draw((x+i*(width/faces.size())), y, width/faces.size(), faces[i].height*(width/faces.size())/faces[i].width);
}

void ofxCvFaceRec::drawHilight(int pnum, int x, int y, int width)
{
    // set properties
    ofNoFill();
    ofSetColor(((0xFF) << pnum)%0xFFFFFF);
    ofSetLineWidth(4.0);

    ofRect(x+pnum*(width/faces.size()), y, width/faces.size(), faces[pnum].height*(width/faces.size())/faces[pnum].width);

    // reset
    ofSetColor(255, 255, 255, 255);

}

void ofxCvFaceRec::drawEigens(int x, int y)
{
    int i;

    for(i=0; i<nEigens; i++)
        eigens[i].draw((x+i*eigens[i].width), y);

}

void ofxCvFaceRec::drawEigens(int x, int y, int width)
{
    int i;

    for(i=0; i<nEigens; i++)
        eigens[i].draw((x+i*(width/nEigens)), y, width/nEigens, eigens[i].height*(width/nEigens)/eigens[i].width);
}

void ofxCvFaceRec::drawPerson(int pnum, int x, int y, int w, int h)
{
    if((pnum < 0) || (pnum > faces.size())) return;

    faces[pnum].draw(x, y, w, h);
}

void ofxCvFaceRec::drawPerson(int pnum, int x, int y)
{
    if((pnum < 0) || (pnum > faces.size())) return;

    faces[pnum].draw(x, y);
}

void ofxCvFaceRec::drawColorPerson(int pnum, int x, int y, int w, int h)
{
    if((pnum < 0) || (pnum > color_faces.size())) return;

    color_faces[pnum].draw(x, y, w, h);
}

void ofxCvFaceRec::drawColorPerson(int pnum, int x, int y)
{
    if((pnum < 0) || (pnum > color_faces.size())) return;

    color_faces[pnum].draw(x, y);
}

unsigned char* ofxCvFaceRec::getPersonPixels(int pnum) {
    if((pnum < 0) || (pnum > color_faces.size())) return NULL;

    return color_faces[pnum].getPixels();
}
void ofxCvFaceRec::addPerson(string name){
	strcpy(newPersonName, name.c_str());
    state = 1;
}
void ofxCvFaceRec::startTraining(){
    state = 3;
}
void ofxCvFaceRec::stopTraining(){
    state = 0;
}

// Perform face detection on the input image, using the given Haar cascade classifier.
// Returns a rectangle for the detected region in the given image.
CvRect ofxCvFaceRec::detectFaceInImage(const IplImage *inputImg, const CvHaarClassifierCascade* cascade )
{
	const CvSize minFeatureSize = cvSize(20, 20);
	const int flags = CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_DO_ROUGH_SEARCH;	// Only search for 1 face.
	const float search_scale_factor = 1.1f;
	IplImage *detectImg;
	IplImage *greyImg = 0;
	CvMemStorage* storage;
	CvRect rc;
	double t;
	CvSeq* rects;
	int i;
    
	storage = cvCreateMemStorage(0);
	cvClearMemStorage( storage );
    
	// If the image is color, use a greyscale copy of the image.
	detectImg = (IplImage*)inputImg;	// Assume the input image is to be used.
	if (inputImg->nChannels > 1) 
	{
		greyImg = cvCreateImage(cvSize(inputImg->width, inputImg->height), IPL_DEPTH_8U, 1 );
		cvCvtColor( inputImg, greyImg, CV_BGR2GRAY );
		detectImg = greyImg;	// Use the greyscale version as the input.
	}
    
	// Detect all the faces.
	t = (double)cvGetTickCount();
	rects = cvHaarDetectObjects( detectImg, (CvHaarClassifierCascade*)cascade, storage,
                                search_scale_factor, 3, flags, minFeatureSize );
	t = (double)cvGetTickCount() - t;
	//printf("[Face Detection took %d ms and found %d objects]\n", cvRound( t/((double)cvGetTickFrequency()*1000.0) ), rects->total );
    
	// Get the first detected face (the biggest).
	if (rects->total > 0) {
        rc = *(CvRect*)cvGetSeqElem( rects, 0 );
    }
	else
		rc = cvRect(-1,-1,-1,-1);	// Couldn't find the face.
    
	//cvReleaseHaarClassifierCascade( &cascade );
	//cvReleaseImage( &detectImg );
	if (greyImg)
		cvReleaseImage( &greyImg );
	cvReleaseMemStorage( &storage );
    
	return rc;	// Return the biggest face found, or (-1,-1,-1,-1).
}
// Returns a new image that is a cropped version of the original image. 
IplImage* ofxCvFaceRec::cropImage(const IplImage *img, const CvRect region)
{
	IplImage *imageTmp;
	IplImage *imageRGB;
	CvSize size;
	size.height = img->height;
	size.width = img->width;
    
	if (img->depth != IPL_DEPTH_8U) {
		printf("ERROR in cropImage: Unknown image depth of %d given in cropImage() instead of 8 bits per pixel.\n", img->depth);
		exit(1);
	}
    
	// First create a new (color or greyscale) IPL Image and copy contents of img into it.
	imageTmp = cvCreateImage(size, IPL_DEPTH_8U, img->nChannels);
	cvCopy(img, imageTmp, NULL);
    
	// Create a new image of the detected region
	// Set region of interest to that surrounding the face
	cvSetImageROI(imageTmp, region);
	// Copy region of interest (i.e. face) into a new iplImage (imageRGB) and return it
	size.width = region.width;
	size.height = region.height;
	imageRGB = cvCreateImage(size, IPL_DEPTH_8U, img->nChannels);
	cvCopy(imageTmp, imageRGB, NULL);	// Copy just the region.
    
    cvReleaseImage( &imageTmp );
	return imageRGB;		
}

// Creates a new image copy that is of a desired size.
// Remember to free the new image later.
IplImage* ofxCvFaceRec::resizeImage(const IplImage *origImg, int newWidth, int newHeight)
{
	IplImage *outImg = 0;
	int origWidth;
	int origHeight;
	if (origImg) {
		origWidth = origImg->width;
		origHeight = origImg->height;
	}
	if (newWidth <= 0 || newHeight <= 0 || origImg == 0 || origWidth <= 0 || origHeight <= 0) {
		printf("ERROR in resizeImage: Bad desired image size of %dx%d\n.", newWidth, newHeight);
		exit(1);
	}
    
	// Scale the image to the new dimensions, even if the aspect ratio will be changed.
	outImg = cvCreateImage(cvSize(newWidth, newHeight), origImg->depth, origImg->nChannels);
	if (newWidth > origImg->width && newHeight > origImg->height) {
		// Make the image larger
		cvResetImageROI((IplImage*)origImg);
		cvResize(origImg, outImg, CV_INTER_LINEAR);	// CV_INTER_CUBIC or CV_INTER_LINEAR is good for enlarging
	}
	else {
		// Make the image smaller
		cvResetImageROI((IplImage*)origImg);
		cvResize(origImg, outImg, CV_INTER_AREA);	// CV_INTER_AREA is good for shrinking / decimation, but bad at enlarging.
	}
    
	return outImg;
}

// Re-train the new face rec database without shutting down.
// Depending on the number of images in the training set and number of people, it might take 30 seconds or so.
CvMat* ofxCvFaceRec::retrainOnline(){
	CvMat *trainPersonNumMat;
	int i;
    
	// Free & Re-initialize the global variables.
	if (faceImgArr) {
		for (i=0; i<nTrainFaces; i++) {
			if (faceImgArr[i])
				cvReleaseImage( &faceImgArr[i] );
		}
	}
	cvFree( &faceImgArr ); // array of face images
	cvFree( &personNumTruthMat ); // array of person numbers
	personNames.clear();			// array of person names (indexed by the person number). Added by Shervin.
	nPersons = 0; // the number of people in the training set. Added by Shervin.
	nTrainFaces = 0; // the number of training images
	nEigens = 0; // the number of eigenvalues
	cvReleaseImage( &pAvgTrainImg ); // the average image
	for (i=0; i<nTrainFaces; i++) {
		if (eigenVectArr[i])
			cvReleaseImage( &eigenVectArr[i] );
	}
	cvFree( &eigenVectArr ); // eigenvectors
	cvFree( &eigenValMat ); // eigenvalues
	cvFree( &projectedTrainFaceMat ); // projected training faces
    
	// Retrain from the data in the files
	printf("Retraining with the new person ...\n");
    
	learn();
	printf("Done retraining.\n");
    
	// Load the previously saved training data
	if( !loadTrainingData( &trainPersonNumMat ) ) {
		printf("ERROR in recognizeFromCam(): Couldn't load the training data!\n");
		exit(1);
	}
	return trainPersonNumMat;
}
// Return a new image that is always greyscale, whether the input image was RGB or Greyscale.
// Remember to free the returned image using cvReleaseImage() when finished.
IplImage* ofxCvFaceRec::convertImageToGreyscale(const IplImage *imageSrc)
{
	IplImage *imageGrey;
	// Either convert the image to greyscale, or make a copy of the existing greyscale image.
	// This is to make sure that the user can always call cvReleaseImage() on the output, whether it was greyscale or not.
	if (imageSrc->nChannels == 3) {
		imageGrey = cvCreateImage( cvGetSize(imageSrc), IPL_DEPTH_8U, 1 );
		cvCvtColor( imageSrc, imageGrey, CV_BGR2GRAY );
	}
	else {
		imageGrey = cvCloneImage(imageSrc);
	}
	return imageGrey;
}