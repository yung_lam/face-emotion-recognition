#pragma once
#include <omp.h>
#include "ofMain.h"
#include "ofxFaceTracker.h"
#include "ofxCvHaarFinder.h"
#include "ofxCvFaceRec.h"
#include "ofxSimpleSerial.h"
#include "ofxUI.h"

#define SCALE 1
#define TEST_DIV 2
#define CAM_WIDTH 640
#define CAM_HEIGHT 480

class testApp : public ofBaseApp {
public:
	void setup();
	void update();
	void draw();
	void keyPressed(int key);
	void guiEvent(ofxUIEventArgs &e);
	void exit(); 
	ofImage img;
	ofImage test_image;
	ofImage bgImage;
	ofImage mask;
	unsigned char *mask_pixels;
	string newPersonName;
	ofxCvHaarFinder finder;
    ofxCvFaceRec rec;	

	ofVideoGrabber cam;
	ofTexture mirrorTexture;
	unsigned char * videoMirror;
	ofxFaceTracker tracker;
	ExpressionClassifier classifier;
	string expressionType;
	ofTexture			videoTexture;
	int 				camWidth;
	int 				camHeight;
	int	countAbnormalPerson;
	int personIndex, primaryExpression;
	ofImage face;
    ofxCvColorImage color;
    ofxCvGrayscaleImage gray;
	ofSoundStream soundStream;
    vector <ofImage> faces;
	ofSoundPlayer  music;
	ofxSimpleSerial serial;
	string	arduinoCmd;
	string	currentPersonName;
	string stablePersonName;
		bool messageSent;
	ofxUICanvas *gui;
	void drawDetectedPerson();
private:
    // vars to toggle onscreen display
    bool showEigens;
    bool showFaces;
    bool showExtracted;
    bool showTest;
    bool showLeastSq;
    bool bgSubtract;
    bool showClock;
	bool isRecognizing;
	bool isExpressionTracking;

};
#pragma once