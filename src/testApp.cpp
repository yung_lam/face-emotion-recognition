 #ifdef _OPENMP
 # include <omp.h>
 #endif
#include "testApp.h"
using namespace ofxCv;
using namespace cv;

void testApp::setup() {
	ofSetLogLevel(OF_LOG_FATAL_ERROR); //set display log level 
	ofSetVerticalSync(true);
	cam.setVerbose(true);
	cam.initGrabber(CAM_WIDTH, CAM_HEIGHT);
	tracker.setup();
	tracker.setRescale(2);
	printf("fan on");
	finder.setup("haarcascade_frontalface_default.xml");
	bgImage.loadImage("FSHPSD1.png");
	rec.learn();
	rec.videoScale=2;
	color.allocate(PCA_WIDTH, PCA_HEIGHT);
	// PRECALCULATE TRANSLUCENT FACE SPRITES
	rec.cascade=finder.cascade;
    showEigens = showFaces = showExtracted = showTest = showLeastSq = showClock = bgSubtract=false;
	isRecognizing = isExpressionTracking=false;
	expressionType="neutral";
	music.setMultiPlay(false);
	music.setVolume(0.8f);
    ofEnableAlphaBlending();
	arduinoCmd = ""; // currentPersonName = "";
	serial.setup("COM3", 9600);
	serial.startContinuousRead(false);
	ofSetFrameRate(30);
	countBeingStable=20;
	//stablePersonName="";
	messageSent = false;
	setupGUI();
	classifier.load("expressions");
	countAbnormalExpression=0;
}

void testApp::update() {
	cam.update();
	if(cam.isFrameNew()) {
		unsigned char * pixels = cam.getPixels();
		color.setFromPixels(pixels, CAM_WIDTH, CAM_HEIGHT);
		gray = color; 
	}
}

void testApp::draw() {
	ofSetColor(255);
	bgImage.draw(0, 0);
	color.draw(105, 220, 640, 480);
	ofPushStyle();
	ofPushMatrix();
	ofTranslate(105, 220);
	tracker.draw();
	ofPopMatrix();
	ofPopStyle();
	vector<int> personSize;

	if (isRecognizing){

		personSize = rec.recognize(gray, &currentPersonName);

		if (currentPersonName.size() > 0){
			printf("currentPersonName ");
			for (int i = 0; i < currentPersonName.size(); i++){
				printf("%s ", currentPersonName[i].c_str());
				bool isInCountNameExistList = false;
				for (int j = 0; j < stablePersonList.size(); j++){
					if (stablePersonList[j].name.compare(currentPersonName[i]) == 0){
						if (stablePersonList[j].count < 40)stablePersonList[j].count++;
						stablePersonList[j].isVisit = true;
						isInCountNameExistList = true;
						if (stablePersonList[j].count == countBeingStable){
							stablePersonList[j].isStable = true;
						}
						break;
					}
				}
				if (!isInCountNameExistList){
					stablePersonList.push_back(countNameExist(1, currentPersonName[i]));
				}
			}
			printf("\n");
		}
		for (int k = 0; k < stablePersonList.size(); k++){
			if (stablePersonList[k].isVisit)stablePersonList[k].isVisit = false;
			else stablePersonList[k].count--;
			if (stablePersonList[k].count <= 0){
				stablePersonList.erase(stablePersonList.begin() + k);
			}
		}
		stablePersonName = "";
		for (int i = 0; i<stablePersonList.size(); i++) {
			if (stablePersonList[i].isStable) stablePersonName = stablePersonName + stablePersonList[i].name + ",  ";
		}
		drawDetectedPerson();
	}
	if (isExpressionTracking){
		if (tracker.update(toCv(gray)))
		classifier.classify(tracker);
		int n = classifier.size();
		if (n>0){
			primaryExpression = classifier.getPrimaryExpression();
			if (expressionType.compare(classifier.getDescription(primaryExpression)) != 0){
				countAbnormalExpression++;
				if (countAbnormalExpression == countBeingStable){
					countAbnormalExpression = 0;
					expressionType = classifier.getDescription(primaryExpression);
					expressionLabel->setLabel("Emotion: " + expressionType);
					if (music.getIsPlaying()) music.stop();
					if (expressionType.compare("angry") == 0){
						if (stablePersonList[0].name.compare("amy") == 0 || stablePersonList[0].name.compare("Amy") == 0){
							if (enterToggle->getValue()){
								serial.writeString("l11");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("f1");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40"); 
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l70");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false);
								EnterLabel->setLabel("Temperature: 25");
								livingLabel->setLabel("Temperature: ");
								WorkLabel->setLabel("Temperature: ");
								}
							else if (workToggle->getValue()){
								serial.writeString("l91");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue Light"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("l71");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("f0");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								WorkLabel->setLabel("Temperature: 26");
								EnterLabel->setLabel("Temperature: ");
								livingLabel->setLabel("Temperature: ");
								}
							//setToggleOnOff("Blue LED", true,"l11"); 
							}
						else if (stablePersonList[0].name.compare("luhan") == 0 || stablePersonList[0].name.compare("Luhan") == 0){
							if (enterToggle->getValue()){
								serial.writeString("l21");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("f1");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l70");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false); 
								EnterLabel->setLabel("Temperature: 24");
								livingLabel->setLabel("Temperature: ");
								WorkLabel->setLabel("Temperature: ");
							}
							else if (workToggle->getValue()){
								serial.writeString("l81");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("l71");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("f0");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false);
								WorkLabel->setLabel("Temperature: 23");
								EnterLabel->setLabel("Temperature: ");
								livingLabel->setLabel("Temperature: ");
							}
						}
						else if (stablePersonList[0].name.compare("kennis") == 0 || stablePersonList[0].name.compare("Kennis") == 0){
							if (enterToggle->getValue()){
								serial.writeString("l11");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue Light"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("f1");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue Light"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l70");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false); 
								EnterLabel->setLabel("Temperature: 24");
								livingLabel->setLabel("Temperature: ");
								WorkLabel->setLabel("Temperature: ");
								}
							else if (workToggle->getValue()){
								serial.writeString("l71");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue Light"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("f0");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false);
								WorkLabel->setLabel("Temperature: 25"); 
								EnterLabel->setLabel("Temperature: ");
								livingLabel->setLabel("Temperature: ");
								}
							}
						else{
							if (enterToggle->getValue()){
								serial.writeString("l21");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue Light"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("f1");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue Light"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l70");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false);
								EnterLabel->setLabel("Temperature: 23.5");
								livingLabel->setLabel("Temperature: ");
								WorkLabel->setLabel("Temperature: ");
								}
							else if (livingToggle->getValue()){
								serial.writeString("l41");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue Light"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("l51");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue Light"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("f0");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l70");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false);
								livingLabel->setLabel("Temperature: 24.5");
								EnterLabel->setLabel("Temperature: ");
								WorkLabel->setLabel("Temperature: ");
								}
							else if (workToggle->getValue()){
								serial.writeString("l71");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue Light"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("l81");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue Light"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("l91");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue Light"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("f0");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								WorkLabel->setLabel("Temperature: 24");
								EnterLabel->setLabel("Temperature: ");
								livingLabel->setLabel("Temperature: ");
								}
							}
							
						}else if (expressionType.compare("happy") == 0){
							if (stablePersonList[0].name.compare("amy") == 0 || stablePersonList[0].name.compare("Amy") == 0){
								if (enterToggle->getValue()){
									serial.writeString("l01");
									serial.writeString("n");
									serial.writeString("f0");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l10");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l20");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l30");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l40");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l50");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l60");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l70");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l80");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l90");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false);
									EnterLabel->setLabel("Temperature: 25");
									livingLabel->setLabel("Temperature: ");
									WorkLabel->setLabel("Temperature: ");

								}else if (workToggle->getValue()){
									serial.writeString("l81");
									serial.writeString("n");
									serial.writeString("l71");
									serial.writeString("f0");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l00");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l10");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l20");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l30");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l40");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l50");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l60");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
									serial.writeString("n");
									serial.writeString("l90");
									//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
									WorkLabel->setLabel("Temperature: 26");
									EnterLabel->setLabel("Temperature: ");
									livingLabel->setLabel("Temperature: ");
								}
								//setToggleOnOff("Blue LED", true, "l11");
						}else if (stablePersonList[0].name.compare("luhan") == 0 || stablePersonList[0].name.compare("Luhan") == 0){
							if (enterToggle->getValue()){
								serial.writeString("l01");
								serial.writeString("n");
								serial.writeString("f0");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l70");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White"); btn->setValue(false);
								serial.writeString("n");
									serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false);
								EnterLabel->setLabel("Temperature: 25");
								WorkLabel->setLabel("Temperature: ");
								livingLabel->setLabel("Temperature: ");
							}
							else if (workToggle->getValue()){
								serial.writeString("l71");
								serial.writeString("n");
								serial.writeString("f0");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false);
								WorkLabel->setLabel("Temperature: 25");
								EnterLabel->setLabel("Temperature: ");
								livingLabel->setLabel("Temperature: ");
							}
						}
						else if (stablePersonList[0].name.compare("kennis") == 0 || stablePersonList[0].name.compare("Kennis") == 0){
							if (enterToggle->getValue()){
								serial.writeString("l31");
								serial.writeString("n");
								serial.writeString("f0");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l70");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false);
								EnterLabel->setLabel("Temperature: 23");
								WorkLabel->setLabel("Temperature: ");
								livingLabel->setLabel("Temperature: ");
							}
							else if (workToggle->getValue()){
								serial.writeString("l71");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue Light"); btn->setValue(true);
								serial.writeString("n");
								serial.writeString("l91");
								serial.writeString("n");
								serial.writeString("f0");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								WorkLabel->setLabel("Temperature: 25.5");
								EnterLabel->setLabel("Temperature: ");
								livingLabel->setLabel("Temperature: ");
							}
						}else {
							if (enterToggle->getValue()){
								serial.writeString("l31");
								serial.writeString("n");
								serial.writeString("f0");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l70");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false);
								EnterLabel->setLabel("Temperature: 24");
								WorkLabel->setLabel("Temperature: ");
								livingLabel->setLabel("Temperature: ");

							}else if (livingToggle->getValue()){
								serial.writeString("l61");
								serial.writeString("n");
								serial.writeString("f0");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l70");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false);
								livingLabel->setLabel("Temperature: 24.5");
								WorkLabel->setLabel("Temperature: ");
								EnterLabel->setLabel("Temperature: ");

							}else if (workToggle->getValue()){
								serial.writeString("l71");
								serial.writeString("n");
								serial.writeString("f0");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Air-conditioner"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l00");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Light Bulb"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l10");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l20");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Green LED"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l30");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Flashing Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l40");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("White Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l50");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Heater"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l60");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange Light"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l80");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Blue"); btn->setValue(false);
								serial.writeString("n");
								serial.writeString("l90");
								//ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget("Orange"); btn->setValue(false);
								WorkLabel->setLabel("Temperature: 25");
								EnterLabel->setLabel("Temperature: ");
								livingLabel->setLabel("Temperature: ");
							}
						}
					}
					else if (expressionType.compare("neutral") == 0){
						serial.writeString("f0");
						serial.writeString("n");
						serial.writeString("l00");
						serial.writeString("n");
						serial.writeString("l10");
						serial.writeString("n");
						serial.writeString("l20");
						serial.writeString("n");
						serial.writeString("l30");
						serial.writeString("n");
						serial.writeString("l40");
						serial.writeString("n");
						serial.writeString("l50");
						serial.writeString("n");
						serial.writeString("l60");
						serial.writeString("n");
						serial.writeString("l70");
						serial.writeString("n");
						serial.writeString("l80");
						serial.writeString("n");
						serial.writeString("l90");
						WorkLabel->setLabel("Temperature: ");
						EnterLabel->setLabel("Temperature: ");
						livingLabel->setLabel("Temperature: ");

					}

					music.loadSound("music/" + stablePersonList[0].name + "-" + expressionType + "-" + roomStr + ".mp3");
					music.play();
				}

				cout << "expressionType" << classifier.getDescription(primaryExpression) << "\n";
			}
			else{
				countAbnormalExpression = 0;
			}
			ofPushStyle();
			ofPushMatrix();
			ofTranslate(50, 95);
			ofFill();
			for (int i = 0; i < n; i++){
				ofSetColor(i == primaryExpression ? ofColor::red : ofColor::black);
				ofRect(0, 0, 100 * classifier.getProbability(i) + .5, 12);
				ofSetColor(255);
				ofDrawBitmapString(classifier.getDescription(i), 5, 9);
				ofTranslate(120, 0);
			}
			ofPopMatrix();
			ofPopStyle();
		}
	}
	drawDetectedPerson();
	currentPersonName.clear();
}


void testApp::keyPressed(int key) {
	
	/*if(key == 'r') {
		tracker.reset();
		classifier.reset();
	}
	if(key == 'e') {
		classifier.addExpression();
	}
	if(key == 'a') {
		classifier.addSample(tracker);
	}
	if(key == 's') {
		classifier.save("expressions");
	}
	if(key == 'l') {
		printf("load classifier");
		classifier.load("expressions");
	}
	 if((key == 'i') || (key == 'I'))
        showEigens = (showEigens == false);

    if((key == 'f') || (key == 'F'))
        showFaces = (showFaces == false);


    if((key == 'q') || (key == 'Q'))
        showLeastSq = (showLeastSq == false);
	
	if (key == 'n'){	
        //rec.addPerson();
    }
    if (key == 't'){	
        rec.startTraining();
		//showTest = (showTest == false);
    }
    if (key == 'q'){	
        rec.stopTraining();        
    }
	if (key == 'z'){
		rec.saveNextFaces = TRUE;
	}*/
	
}


void testApp::exit()
{
	serial.writeString("f0");
	serial.writeString("n");
	serial.writeString("l00");
	serial.writeString("n");
	serial.writeString("l10");
	serial.writeString("n");
	serial.writeString("l20");
	serial.writeString("n");
	serial.writeString("l30");
	serial.writeString("n");
	serial.writeString("l40");
	serial.writeString("n");
	serial.writeString("l50");
	serial.writeString("n");
	serial.writeString("l60");
	serial.writeString("n");
	serial.writeString("l70");
	serial.writeString("n");
	serial.writeString("l80");
	serial.writeString("n");
	serial.writeString("l90");
    gui->saveSettings("GUI/guiSettings.xml");     

    delete gui; 
}

void testApp::guiEvent(ofxUIEventArgs &e)
{
   if(e.widget->getName() == " Identity Recognition"){
		ofxUIToggle *btn = (ofxUIToggle *) e.widget;    
		isRecognizing = btn->getValue();
	}else if (e.widget->getName() == "New User"){
		ofxUIButton *btn = (ofxUIButton *)e.widget;
		if (btn->getValue() == true){
			gui->addWidget(NameInput);
			gui->addWidget(new ofxUIButton(1600, 180, 30, 30, false, "Confirm"));
		}
	//Entertainment room
	}else if (e.widget->getName() == "Air-conditioner"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue() == true){
			serial.writeString("f1");
		}else{
			serial.writeString("f0");
		}
	}
	else if (e.widget->getName() == "Light bulb"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue() == true){
			serial.writeString("l01");
		}
		else{
			serial.writeString("l00");
		}
	}else if (e.widget->getName() == "Blue LED"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue() == true){
			serial.writeString("l11");
		}
		else{
			serial.writeString("l10");
		}
	}
	else if (e.widget->getName() == "Flashing light"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue() == true){
			serial.writeString("l31");
		}
		else{
			serial.writeString("l30");
		}
	
	}else if (e.widget->getName() == "Green LED"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue() == true){
			serial.writeString("l21");
		}
		else{
			serial.writeString("l20");
		}

		//Living room
	}else if (e.widget->getName() == "White Light"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue()){
			serial.writeString("l41");
		}
		else{
			serial.writeString("l40"); 
		} 	
	}else if (e.widget->getName() == "Heater"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue()){
			serial.writeString("l51");
		}
		else{
			serial.writeString("l50");
		}
	}
	else if (e.widget->getName() == "Orange Light"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue()){
			serial.writeString("l61");
		}
		else{
			serial.writeString("l60");
		}
		//Workplace
	}else if (e.widget->getName() == "White"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue()){
			serial.writeString("l71");
		}
		else{
			serial.writeString("l70");
		}
	}else if (e.widget->getName() == "Orange"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue()){
			serial.writeString("l91");
		}
		else{
			serial.writeString("l90");
		}
	}else if (e.widget->getName() == "Blue"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue()){
			serial.writeString("l81");
		}
		else{
			serial.writeString("l80");
		}
	}
	
	else if (e.widget->getName() == "Confirm"){
		ofxUIButton *btn = (ofxUIButton *) e.widget;  
		if(btn->getValue()==true){
			printf("Name %s\n", newPersonName.c_str());
			rec.addPerson(newPersonName);
			//rec.startTraining();
			isRecognizing = true;
			gui->addWidget(new ofxUIButton(1600, 180, 30, 30, false, "Add Face"));
			gui->addWidget(new ofxUIButton(1600, 230, 30, 30, false, "Finish Face Adding"));
			//btn->setVisible(false);
			//btn->removeWidget();
			gui->removeWidget(btn);
			gui->removeWidget(NameInput);
		}
		//gui->removeWidget(btn);
	}else if(e.widget->getName()=="NameInput"){
		ofxUITextInput *input=(ofxUITextInput *)e.widget;
		newPersonName=input->getTextString();
		
	}else if(e.widget->getName()=="Add Face"){
		ofxUIButton *btn = (ofxUIButton *) e.widget;  
		if(btn->getValue()==true){
			rec.saveNextFaces = TRUE;
		}
	}else if(e.widget->getName()=="Finish Face Adding"){
		ofxUIButton *btn = (ofxUIButton *) e.widget;  
		if(btn->getValue()==true){
			ofxUIButton *btn=(ofxUIButton *)gui->getWidget("Add Face");
			gui->removeWidget(btn);
			ofxUIButton *btnF=(ofxUIButton *)gui->getWidget("Finish Face Adding");
			gui->removeWidget(btnF);
			ofxUITextInput *input=(ofxUITextInput *)gui->getWidget("NameInput");
			//gui->removeWidget(input);
			rec.startTraining();
		}
	}else if(e.widget->getName()==" Emotion Recognition"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if(btn->getValue()==true){
			isExpressionTracking = true; 
		}
		else{
			isExpressionTracking = false;
			expressionLabel->setLabel("Emotion: ");
		}
	}
	else if (e.widget->getName() == "Level of confidence"){
		ofxUISlider *btn = (ofxUISlider *)e.widget;
		rec.fConfidence = btn->getScaledValue();
	
	}
	else if (e.widget->getName() == "Entertainment Room"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue()){
			roomStr = "en";
			 workToggle->setValue(false);
			// enterToggle;
			 livingToggle->setValue(false);
		}  
	}
	else if (e.widget->getName() == "Living Room"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue()){
			roomStr = "living";
			workToggle->setValue(false);
			// enterToggle;
			enterToggle->setValue(false);
		}
	}
	else if (e.widget->getName() == "Workplace"){
		ofxUIToggle *btn = (ofxUIToggle *)e.widget;
		if (btn->getValue()){
			roomStr = "work";
			livingToggle->setValue(false);
			// enterToggle;
			enterToggle->setValue(false);
		}
	}
	else if (e.widget->getName() == "Music Stop"){
		ofxUIButton *btn = (ofxUIButton *)e.widget;
		if (btn->getValue() == true){
			if (music.getIsPlaying()) music.stop();
		}
		//gui->removeWidget(btn);
	}
}

 
void testApp::drawDetectedPerson()
{
	ofDrawBitmapString(ofToString((int) ofGetFrameRate()), ofGetWidth() - 20, ofGetHeight() - 10);

	confirmPersonLabel->setLabel(stablePersonName);
	/*drawHighlightString(
		string() +
		"r - reset\n" +
		"e - add expression\n" +
		"a - add sample\n" +
		"s - save expressions\n"
		"l - load expressions",
		14, ofGetHeight() - 7 * 12); 
	drawHighlightString(
	"Detected Person:",
	20, 520);
	
	for(int i=0;i<rec.nPersons;i++){
	rec.personNames;
	}*/
}

void testApp::setupGUI(){
	gui = new ofxUICanvas(0,0,1920,1200);	
	gui->addWidget(new ofxUIToggle(930, 505, 30, 30, false, " Identity Recognition", OFX_UI_FONT_LARGE));
	gui->addWidget(new ofxUIToggle(930, 449, 30, 30, false, " Emotion Recognition", OFX_UI_FONT_LARGE));
	gui->addWidget(new ofxUIButton(1600, 100, 30, 30, false, "New User", OFX_UI_FONT_LARGE));
	 
	/* Entertainment Room*/  
	enterToggle = new ofxUIToggle(1300, 100, 30, 30, false, "Entertainment Room", OFX_UI_FONT_LARGE);
	gui->addWidget(enterToggle);
	EnterLabel=new ofxUILabel(1370, 140,"EnterLabel", "Temperature: ",OFX_UI_FONT_MEDIUM);
	gui->addWidget(EnterLabel);
	
	gui->addWidget(new ofxUIToggle(1370, 180, 20, 20, false, "Light bulb", OFX_UI_FONT_MEDIUM));
	gui->addWidget(new ofxUIToggle(1370, 210, 20, 20, false, "Blue LED", OFX_UI_FONT_MEDIUM));
	gui->addWidget(new ofxUIToggle(1370, 240, 20, 20, false, "Flashing light", OFX_UI_FONT_MEDIUM));
	gui->addWidget(new ofxUIToggle(1370, 270, 20, 20, false, "Green LED", OFX_UI_FONT_MEDIUM));
	gui->addWidget(new ofxUIToggle(1370, 300, 20, 20, false, "Air-conditioner", OFX_UI_FONT_MEDIUM));
	
	/* Living Room*/ //ofLine(1300, 340, 1660, 340);
	

	livingToggle = new ofxUIToggle(1300, 380, 30, 30, false, "Living Room", OFX_UI_FONT_LARGE);
	gui->addWidget(livingToggle);
	livingLabel = new ofxUILabel(1370, 420, "livingLabel", "Temperature: ", OFX_UI_FONT_MEDIUM);
	gui->addWidget(livingLabel);

	gui->addWidget(new ofxUIToggle(1370, 450, 20, 20, false, "White Light", OFX_UI_FONT_MEDIUM));
	gui->addWidget(new ofxUIToggle(1370, 480, 20, 20, false, "Heater", OFX_UI_FONT_MEDIUM));
	gui->addWidget(new ofxUIToggle(1370, 510, 20, 20, false, "Orange Light", OFX_UI_FONT_MEDIUM));
	//setToggleOnOff("Orange LED",true,"l61");

	/* Workplace*/  //ofLine(580, 840, 580, 1000);

	workToggle = new ofxUIToggle(1300, 590, 30, 30, false, "Workplace", OFX_UI_FONT_LARGE);
	gui->addWidget(workToggle);
	WorkLabel = new ofxUILabel(1370, 630, "WorkLabel", "Temperature: ", OFX_UI_FONT_MEDIUM);
	gui->addWidget(WorkLabel );

	gui->addWidget(new ofxUIToggle(1370, 660, 20, 20, false, "White", OFX_UI_FONT_MEDIUM));
	gui->addWidget(new ofxUIToggle(1370, 690, 20, 20, false, "Orange", OFX_UI_FONT_MEDIUM));
	gui->addWidget(new ofxUIToggle(1370, 720, 20, 20, false, "Blue", OFX_UI_FONT_MEDIUM));
	//gui->addWidget(new ofxUIToggle(1370, 680, 20, 20, false, "Window", OFX_UI_FONT_MEDIUM));

	gui->addWidget(new ofxUIButton(1600, 460, 20, 20, false, "Music Stop", OFX_UI_FONT_LARGE));
	gui->addWidget(new ofxUISlider("Level of confidence", 0.0, 1.0, &rec.fConfidence, 720, 8, 50, 880));
	//gui->addWidgetDown(new ofxUITextInput());
	ofAddListener(gui->newGUIEvent, this, &testApp::guiEvent); 
	gui->loadSettings("GUI/guiSettings.xml"); 
	confirmPersonLabel = new ofxUILabel(190, 785, "confirmPersonLabel", " ", OFX_UI_FONT_LARGE); //"James "
	gui->addWidget(confirmPersonLabel);
	expressionLabel = new ofxUILabel(50, 60, "expressionLabel", "Emotion: ", OFX_UI_FONT_LARGE);
	gui->addWidget(expressionLabel);

	NameInput = new ofxUITextInput(1600, 140, 180, "NameInput", "NewUserName", OFX_UI_FONT_MEDIUM);
}

void testApp::setToggleOnOff(string id, bool val, string serialCmd){
	ofxUIToggle *btn = (ofxUIToggle *)gui->getWidget(id);
	btn->setValue(val);
	serial.writeString(serialCmd);
}